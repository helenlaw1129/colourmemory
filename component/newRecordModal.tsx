import React, { useState } from 'react';
import { Modal, View, Text, StyleSheet, Button, TouchableOpacity } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { normalize } from './NavBar';
import AsyncStorage from '@react-native-community/async-storage';


const NewRecordModal = (props: any) => {
    const [name, onChangeName] = useState('')
    const navigation = useNavigation()
    const gameData = {
        Player: name,
        Score: props.score
    }

    async function setHighScore() {
        try {
            await AsyncStorage.setItem(props.score.toString(), JSON.stringify(gameData));
        } catch (e) {
            console.log(e);
        }
        console.log(gameData);
    }

    return (
        <Modal visible={props.modalOn} animationType={"slide"} transparent={true}>
            <View style={styles.centerContainer}>
                <View style={styles.modalContainer}>
                    <Text style={styles.modalTitle}>Your score is {props.score}</Text>
                    <Text style={styles.inputName}>Please input your name:</Text>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={name => onChangeName(name)}
                        value={name}
                        maxLength={30}
                    />
                    <TouchableOpacity onPress={() => {
                        setHighScore(),
                            props.onHighScoreUpdate(),
                            setTimeout(() => { navigation.navigate('Record'), props.onResetScore(), props.onModalOff() }, 1000)
                    }}
                        style={styles.button}>
                        <Text style={styles.submit}>Submit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )

}

const styles = StyleSheet.create({
    centerContainer: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalContainer: {
        width: "80%",
        height: '55%',
        backgroundColor: '#FFF',
        borderColor: '#FFF',
        borderWidth: 1,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.5,
        elevation: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalTitle: {
        fontSize: normalize(18),
        fontWeight: '900',
        marginBottom: '5%',
        color: "#1485F3"
    },
    inputName: {
        fontSize: normalize(12),
        fontWeight: '500',
        marginBottom: '5%',
        color: "#1485F3"
    },
    textInput: {
        width: '40%',
        height: '10%',
        borderColor: '#000',
        borderWidth: 1,
        borderRadius: 10,
        fontSize: normalize(14),
        paddingLeft: '3%'
    },
    button: {
        width: "35%",
        justifyContent: 'center',
        alignItems: 'center',
        padding: '3%',
        marginTop: '10%',
        borderColor: '#FFF',
        borderWidth: 1,
        borderRadius: 10,
        backgroundColor: "#1485F3"
    },
    submit: {
        color: '#FFF',
        fontWeight: '700',
        fontSize: normalize(12)
    }
})

export default NewRecordModal;