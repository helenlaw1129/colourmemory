import React from 'react';
import { Modal, Text, StyleSheet, View } from 'react-native';
import { normalize } from './NavBar';

const GameModal = (props:any)=>{
    return(
        <Modal visible={props.modalOn} animationType={"slide"} transparent={true}>
            <View style={styles.centerContainer}>
                <View style={styles.messageContainer}>
                <Text style={styles.message}>{props.message}</Text>
            </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    centerContainer: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    messageContainer:{
        width: '50%',
        height: '30%',
        borderRadius: 20,
        backgroundColor: 'rgba(255,255,255,0.7)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    message:{
        fontSize: normalize(20),
        fontWeight: '900',
        color: '#000',
        textShadowColor:'#6ab4ff',
        textShadowRadius: 5,
        textShadowOffset: {width: 5, height: 5},
        elevation: 8
    }
})

export default GameModal;
