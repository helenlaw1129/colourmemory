import React, {useEffect } from 'react';
import { View, Text, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { normalize } from './NavBar';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';


const RecordScreen = ({reduxHighScore}:any) => {

    const navigation= useNavigation();

    const clearData = async()=>{
        await AsyncStorage.clear();
    }

    return (
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <View style={styles.scoreContainer}>
            <View style={{width: '100%', flex:1, alignItems:'flex-end'}}>
            <View
                style={[{ width: '15%' }, {height: '10%'},{flex: 1}, {alignContent:'center'}, {justifyContent: 'center'}]}>
            {/* <TouchableOpacity
                onPress={() => clearData()}
            >
                <Text>Clear</Text>
            </TouchableOpacity> */}

            <TouchableOpacity
                onPress={() => navigation.navigate('Board')}
            >
                <Text style={{fontWeight:'900', fontSize: normalize(10)}}>Back</Text>
            </TouchableOpacity>
            </View>
            </View>
            <View style={styles.highScoreContainer}>
                <Text style={styles.highScoreTitle}>High Score</Text>
            </View>
            <View style={styles.recordRowContainer}>
                       <View style={[styles.recordRow, {backgroundColor:'#6ab4ff'}]}>
                            <View style={styles.recordId}>
                            <Text style={styles.recordFont}>Rank</Text>
                            </View>
                            <View style={styles.recordData}>
                            <Text style={styles.recordFont}>Player</Text>
                            </View>
                            <View style={styles.recordData}>
                            <Text style={styles.recordFont}>Score</Text>
                            </View>
                        </View>
                {reduxHighScore?.map((record: any, index:any) => {
                    return (
                        <View key={index}style={styles.recordRow}>
                            <View style={styles.recordId}>
                            <Text style={styles.recordFont}>{index+1}</Text>
                            </View>
                            <View style={styles.recordData}>
                            <Text style={[styles.recordFont, {fontWeight:'500'}]}>{record.Player}</Text>
                            </View>
                            <View style={styles.recordData}>
                            <Text style={[styles.recordFont, {fontWeight:'500'}]}>{record.Score}</Text>
                            </View>
                        </View>
                    )
                })}
            </View>
        </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    scoreContainer: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        alignItems:'center',
        flex: 15,
        borderWidth: normalize(6),
        borderColor: '#1485F3',
        borderRadius: 20
    },
    highScoreContainer:{
        flex:2,
        justifyContent: 'center'
    },
    highScoreTitle:{
        fontSize: normalize(20),
        fontWeight: '900'
    },
    recordRowContainer:{
        flex: 12,
    },
    recordRow:{
        width: '90%',
        borderWidth: 1,
        borderColor: '#1485F3',
        borderRadius: 20,
        flexDirection:'row',
        justifyContent:'center',
        margin: '1%',
        paddingTop: '1%',
        paddingBottom: '1%',
    },
    recordId:{
        width: '20%',
        paddingLeft: '5%',
    },
    recordData:{
        width:'40%',
        paddingLeft: '5%',
    },
    recordFont:{
        fontSize: normalize(8),
        fontWeight: '800'
    }

})

const mapStateToProps = (state:any) =>{
    console.log('reduxState is')
    console.log(state)
    return {reduxHighScore:state};
  }
export default connect(mapStateToProps)(RecordScreen);
