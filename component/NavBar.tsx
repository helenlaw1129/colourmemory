import React from 'react';
import {  View, StyleSheet, Text, Image, Dimensions, Platform, PixelRatio } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';


const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  } = Dimensions.get('window');
  
  // based on iphone 5s's scale
  const scale = SCREEN_WIDTH / 320;
  
  export function normalize(size:any) {
    const newSize = size * scale 
    if (Platform.OS === 'ios') {
      return Math.round(PixelRatio.roundToNearestPixel(newSize))
    } else {
      return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    }
  }

interface PropsState{
  currentScore: number;
}

const NavBar = (props: PropsState) => {
  const navigation = useNavigation();

    return (
      <>
        <View style = {styles.navBar}>
          <View style={styles.logoContainer}>
            <Image
              style= {styles.logo}
              source={require("../Assets/appItIcon.jpeg")}/>
          </View>
          <View style = {styles.currentScoreContainer}>
            <Text style={styles.currentScore}>Current Score: {props.currentScore}</Text>
          </View>
          <View style={styles.HighScoreContainer} >
          <TouchableOpacity onPress={()=>navigation.navigate('Record')}>
            <Text style={styles.HighScore}>High Score</Text>
          </TouchableOpacity>
          </View>
  
        </View>

      </>
    );
  };

  const styles = StyleSheet.create({
    navBar:{
      backgroundColor:"#1485F3",
      width: "100%",
      height: "9%",
      display: "flex",
      flexDirection:'row',
      justifyContent: 'space-between',
    },
    logoContainer:{
      // marginTop: "5%",
      width: "25%",
      justifyContent: "center",
      alignItems: "center",
    },
    logo:{
      width: "100%",
      height: "100%",
    },
    currentScoreContainer:{
      width: '50%',
      justifyContent: "center",
      alignItems: "center",
    },
    currentScore:{
      fontSize: normalize(14),
      color:"white"
    },
    HighScoreContainer:{
      // marginTop: '5%',
      width: '25%',
      justifyContent: "center",
      alignItems: "center",
    },
    HighScore:{
      fontWeight: '800',
      color: 'white',
      fontSize: normalize(13)
    }
  
  });

export default NavBar;