import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
} from 'react-native';

import NavBar from './NavBar';
import Board from './Board';
import AsyncStorage from '@react-native-community/async-storage';
import { store } from '../App';
import { useSelector, connect } from 'react-redux';

const Game = ({reduxHighScore}:any) => {
  const cards = [
    {
      'id': '1',
      'value': 'red',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '2',
      'value': 'red',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '3',
      'value': 'blue',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '4',
      'value': 'blue',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '5',
      'value': 'yellow',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '6',
      'value': 'yellow',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '7',
      'value': 'green',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '8',
      'value': 'green',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '9',
      'value': 'purple',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '10',
      'value': 'purple',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '11',
      'value': 'orange',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '12',
      'value': 'orange',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '13',
      'value': 'black',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '14',
      'value': 'black',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '15',
      'value': 'white',
      'isActive': false,
      'isMatched': false
    },
    {
      'id': '16',
      'value': 'white',
      'isActive': false,
      'isMatched': false
    },
  ]

  const [currentScore, setScore] = useState(0);
  const [highScore, setHighScore1] = useState<any>([])
  const [times, setTimes] = useState(0)
  const [newGame, setNewGame] = useState(cards)


  const shuffleCards = (cardsArr: object[]) => {
    let newArr: any = [];
    for (let i = cardsArr.length - 1; i >= 0; i--) {
      let index = Math.floor(Math.random() * (cardsArr.length));
      newArr.splice(index, 0, cardsArr[i]);
    }
    return newArr
  }

  const getHighScore = async () => {
    const highscoreKey = await AsyncStorage.getAllKeys();

    // const clearAll = await AsyncStorage.clear()
    if (highscoreKey) {
      highscoreKey.sort(function (a, b) { return parseInt(a) - parseInt(b) }).reverse();
      let newArr: any = []
      highscoreKey.map(async (key, index) => {
        const highscore = await AsyncStorage.getItem(key.toString());
        newArr.push(JSON.parse(highscore))
      })
      setTimeout(() => {
        setHighScore1(newArr)
        store.dispatch({type:'@@setHighScore', highScore: newArr})
      }, 500)
    }
    setTimes(times + 1)
    console.log('function times is ' + times)
  }

  const handleScoreUpdate = (number: number) => {
    setScore(currentScore + (number));
  }

  const handleResetScore = () => {
    setScore(0);
  }

  const handleResetGame = () => {
    setNewGame(shuffleCards(cards))
  }

  useEffect(() => {
    getHighScore()
    handleResetGame();
  }, [])



  return (
    <>
      <SafeAreaView style={{ width: '100%', height: '100%' }}>
        <NavBar currentScore={currentScore} />
        <Board onScoreUpdate={handleScoreUpdate}
          onResetScore={handleResetScore}
          score={currentScore}
          onHighScoreUpdate={getHighScore}
          
          newGame={newGame}
          onResetGame={handleResetGame}
        />
      </SafeAreaView>
    </>
  );
};




export default Game;
