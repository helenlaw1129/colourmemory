import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import NewRecordModal from './newRecordModal';
import AsyncStorage from '@react-native-community/async-storage';
import GameModal from './GameModal';

interface FlipState {
  value: string,
  id: string,
  index: number | null
}

interface PropsState {
  onScoreUpdate: any;
  onResetScore: any;
  onHighScoreUpdate: any;
  score: number;
  newGame: any;
  onResetGame: any;
}

const Board = (props: PropsState) => {

  const [firstFlip, setFirstFlip] = useState<FlipState>({ value: '', id: '', index: null });
  const [secondFlip, setSecondFlip] = useState<FlipState>({ value: '', id: '', index: null });
  const [attempt, setAttempt] = useState(0);
  const [matchedPair, setMatchedPair] = useState(0);
  const [recordModal, setRecordModal] = useState(false);
  const [gameModal, setGameModal] = useState(false);
  const [message, setMessage] = useState('')


  function checkFlip(inputValue: string, inputId: string, inputIndex: number) {
    if (firstFlip.id == '') {
      setFirstFlip({ value: inputValue, id: inputId, index: inputIndex });
    } else if (firstFlip.id == inputId) {
      return console.log('you have pressed the same card');
    } else {
      setSecondFlip({ value: inputValue, id: inputId, index: inputIndex })
      setAttempt(attempt + 1)
    }
    props.newGame[inputIndex].isActive = true;
    // console.log(props.newGame[inputIndex].isActive)
  }

  const handleModalOff = () => {
    setRecordModal(false)
  }

  useEffect(() => {
    function matchFlip() {
      if (firstFlip.value !== '' && secondFlip.value !== '') {
        if (firstFlip.value == secondFlip.value) {
          setMatchedPair(matchedPair + 1);
          props.onScoreUpdate(5);
          setMessage('Matched! +5');
          setGameModal(true);
          setTimeout(() => {
            firstFlip.index !== null ? props.newGame[firstFlip.index].isMatched = true : console.log("First index = null");
            secondFlip.index !== null ? props.newGame[secondFlip.index].isMatched = true : console.log("Second index = null");
          }, 800)

          setTimeout(() => {
            setGameModal(false)
          }, 1000)
        } else {
          props.onScoreUpdate(-1);
          setMessage('Wrong! -1')
          setGameModal(true);
          setTimeout(() => {
            firstFlip.index !== null ? props.newGame[firstFlip.index].isActive = false : console.log("First index = null");
            secondFlip.index !== null ? props.newGame[secondFlip.index].isActive = false : console.log("Second index = null");
          }, 800)

          setTimeout(() => {
            setGameModal(false)
          }, 1000)
        }
      }
      setFirstFlip({ value: '', id: '', index: null });
      setSecondFlip({ value: '', id: '', index: null });
    }
    matchFlip();
  }, [attempt]);

  useEffect(() => {
    const checkHighScore = async () => {
      const highScore = await AsyncStorage.getAllKeys();
      highScore.sort(function (a, b) { return parseInt(a) - parseInt(b) }).reverse()
      console.log('Current score is ' + props.score)
      if (matchedPair === 8) {
        setMatchedPair(0);
        if (highScore[0] == null) {
          setTimeout(() => {
            props.onResetGame();
            setRecordModal(true);
          }, 1300)
        } else {
          console.log('record is ' + parseInt(highScore[0]))
          if (props.score > parseInt(highScore[0])) {
            setTimeout(() => {
              props.onResetGame();
              setRecordModal(true);
            }, 1300)
          }else(
            setTimeout(() => {
              props.onResetGame();
              props.onResetScore();
            }, 1300)
          )
        }
      }
    }
    checkHighScore();
  }, [matchedPair === 8])

  useEffect(() => {
    props.onResetScore();
  }, [])

  return (
    <>
      <View style={styles.boardContainer}>
        <View style={styles.board}>
          <View style={styles.cardsContainer}>
            {props.newGame.map((card: any, index: any) => {
              let value: string = card.value;
              if (card.isMatched) {
                return <View key={index} style={styles.matchedCard}></View>
              } else if (card.isActive) {
                return (
                  <View key={index}
                    style={[styles.card, { backgroundColor: card.value }]} >
                  </View>
                )
              } else {
                return (
                  <TouchableOpacity key={index}
                    style={styles.card}
                    onPress={() => { checkFlip(card.value, card.id, index) }} >
                    <Image source={require('../Assets/flipMe.png')} style={styles.image} />
                  </TouchableOpacity>
                )
              }
            }
            )}

          </View>
        </View>
      </View>
      <GameModal modalOn={gameModal} message={message} />
      <NewRecordModal modalOn={recordModal} score={props.score} onModalOff={handleModalOff} onHighScoreUpdate={() => { props.onHighScoreUpdate() }} onResetScore={() => { props.onResetScore() }} />

    </>
  )
}


const styles = StyleSheet.create({
  boardContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  board: {
    width: '90%',
    height: '90%',
    borderRadius: 10,
    backgroundColor: 'rgba(0,0,0,0.3)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cardsContainer: {
    width: '90%',
    height: '90%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignContent: 'space-around'
  },
  card: {
    width: '22%',
    height: '21%',
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#6ab4ff'
  },
  image: {
    width: "100%",
    resizeMode: 'contain'
  },
  matchedCard: {
    width: '22%',
    height: '21%',
    justifyContent: 'center',
    alignItems: 'center',

  }

});

export default Board;