# Colour Memory


## Getting Started


Then follow these steps to install and run the game:

1. Clone this repo to your local machine.
2. Run `yarn` or `npm` to install the dependencies.
3. Go to ios folder and link the dependencies.
    ```bash
    cd ios && pod install
    ```
4. Now `cd` to the root directory and start the application.
    ```bash
    yarn react-native start
    ```
5. Run simulator or devices using `Xcode`(iOS) or `Android Studio`(Android).
6. Enjoy the game!
