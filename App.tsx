/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import RecordScreen from './component/recordScreen';
import {createStackNavigator} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Game from './component/Game';
import {createStore} from 'redux';
import { Provider } from 'react-redux';

const initialStore:any={
  highScore: []
}

function reducer(state:any, action:any){
  if(action.type ==='@@setHighScore'){
    {state=action.highScore}
  }
  return state
}
export const store = createStore(reducer,initialStore);
// store.dispatch({type:'@@setHighScore', highScore: '[{"Player":"ABC","Score":0}]'})



const RootStack = createStackNavigator();

const App = () => {

  return (
    <>
    <Provider store={store}>
    <NavigationContainer>
      <RootStack.Navigator initialRouteName="Board" screenOptions={{headerShown:false}}>
        <RootStack.Screen name ="Board" component={Game}/>
        <RootStack.Screen name ="Record" component={RecordScreen}/>
      </RootStack.Navigator>
    </NavigationContainer>
    </Provider>
    </>
  );
};



export default App;
